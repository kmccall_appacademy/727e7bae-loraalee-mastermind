class Code
  attr_reader :pegs

  PEGS = {
    "R" => "Red",
    "G" => "Green",
    "B" => "Blue",
    "Y" => "Yellow",
    "O" => "Orange",
    "P" => "Purple"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(guess_code)
    guess_pegs = []

    guess_code.each_char do |peg|
      guess_pegs << peg.upcase
    end

    if guess_pegs.all? { |peg| PEGS.include?(peg) }
      Code.new(guess_pegs)
    else
      raise error
    end
  end

  def self.random
    random_pegs = []

    PEGS.to_a.sample(4).each do |color|
      random_pegs << color.first
    end

    Code.new(random_pegs)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(code)
    matches = 0

    (0..3).each do |i|
      matches += 1 if code[i] == pegs[i]
    end

    matches
  end

  def near_matches(code)
    near_matches = 0
    repeats = []

    (0..3).each do |i|
      peg = code[i]
      if pegs.include?(peg) && pegs[i] != peg && !repeats.include?(peg)
        near_matches += 1
      end
      repeats << code[i]
    end

    near_matches
  end

  def ==(code)
    if exact_matches(code) == 4
      true
    else
      false
    end
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def play
    puts "Welcome to Mastermind!"

    guess = get_guess

    until guess == secret_code
      self.display_matches(guess)
      puts "Try again"
      guess = get_guess
    end

    puts "Congratulations, you win!"
  end

  def get_guess
    puts "Guess a code (four of the following letters: R G B Y O P)"
    guess = gets.chomp

    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end
end

if $PROGRAM_NAME == __FILE__
  Game.new.play
end
